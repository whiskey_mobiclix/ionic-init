import "./reset.scss";
import React from 'react';
import App from './app/routes';
import { hydrate } from 'react-dom';
import store from './@moco-core/redux/core-store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

const renderApp = Comp =>
    hydrate(
        <BrowserRouter>
            <Provider store={store}>
                <Comp />
            </Provider>
        </BrowserRouter>,
        document.getElementById('root')
    );

renderApp(App);
serviceWorker.unregister();
